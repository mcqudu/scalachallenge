#Cube Permutations

This code base will return the smallest cube whose digits can be permuted
to form exactly 5 other cubes.

##Algorithm
The algorithm works by realizing that the shared digits in the cubes are
anagrams. Since the 5 cubes all have the same digits just in different
orders the algorithm generates cubes then takes the digits of the cube,
converts them to a String, converts the String to an Array, sorts the Array,
then converts the Array to a String. This String is then used as a key to 
Map from Strings to Arrays of BigInts. The BigInts are the roots of the cubes.
The algorithm stops when a key maps to an Array of exactly 5 elements, those
5 elements are the roots of the 5 cubes whose digits can be permuted into each
other. The 0th element in this array is the smallest cube whose digits can be
permuted into 5 other cubes.

##Running the code
To run the code in OS X simply run:

    ./run.sh

In the terminal within the checked out folder.

In Windows run:

    run.bat

The project is built with Maven and it will download all dependencies and
create a jar and run it.