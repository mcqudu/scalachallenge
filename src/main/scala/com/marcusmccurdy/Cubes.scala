/**
 * User: Marcus McCurdy
 * Date: 3/29/14
 */
package com.marcusmccurdy

import scala.collection.mutable

object Cubes {

  /**
   * Cubes a BigInt.
   * @param x BigInt to be cubed.
   * @return the cube of x.
   */
  def cube(x: BigInt) = x * x * x

  /**
   * Takes a BigInt and returns a sorted String of its digits.
   * @param x BigInt to be sorted.
   * @return a sorted String of the digits of x.
   */
  def toSortedString(x: BigInt): String = {
    x.toString().toArray.sorted.mkString("")
  }

  /**
   * Finds the smallest cube for which exactly n of its digits are cubes.
   * @param n the number of permutations that should be cubes.
   * @return A Tuple2[BigInt, BigInt] of the cube and its root.
   */
  def findSmallestCubePermutations(n: Int): Tuple2[BigInt, BigInt] = {
    val sortedCubeToCubeRoots = mutable.Map.empty[String, Array[BigInt]]
    var i = BigInt(n)
    var updatedRoots = Array.empty[BigInt]
    while (updatedRoots.length < n) {
      val currentCube = cube(i)
      val key = toSortedString(currentCube)
      if (sortedCubeToCubeRoots.get(key) == None) {
        sortedCubeToCubeRoots(key) = Array(i)
      } else {
        val currentRoots = sortedCubeToCubeRoots.get(key).get
        updatedRoots = currentRoots :+ i
        sortedCubeToCubeRoots(key) = updatedRoots
      }
      i += 1
    }
    val smallestRoot = updatedRoots(0)
    val smallestCube = cube(smallestRoot)
    (smallestCube, smallestRoot)
  }

  def main(args: Array[String]) {
    val (smallestCube, smallestRoot) = findSmallestCubePermutations(5)
    println(s"The smallest cube for which exactly five permutations " +
      s"of its digits are cube is $smallestCube ($smallestRoot^3)")
  }
}
