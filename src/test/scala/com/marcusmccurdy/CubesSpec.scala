package com.marcusmccurdy
import org.scalatest.FlatSpec

import com.marcusmccurdy.Cubes._

class CubesSpec extends FlatSpec {
  "Integer 312 to sorted string" should "be 123" in {
    assert(toSortedString(BigInt(312)) == "123")
  }

  "1 cubed" should "be 1" in {
    assert(cube(1) == BigInt(1))
  }

  "2 cubed" should "be 8" in {
    assert(cube(BigInt(2)) == BigInt(8))
  }

  "4 cubed" should "be 64" in {
    assert(cube(BigInt(4)) == BigInt(64))
  }

  "The smallest cube for which exactly 2 of its permutations are cube" should "be (125, 5)" in {
    assert(findSmallestCubePermutations(2) == (BigInt(125), BigInt(5)))
  }

  "The smallest cube for which exactly 3 of its permutations are cube" should "be (41063625, 345)" in {
    assert(findSmallestCubePermutations(3) == (BigInt(41063625), BigInt(345)))
  }
}
